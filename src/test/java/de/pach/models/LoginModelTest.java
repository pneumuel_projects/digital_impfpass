/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.controller.LoginController;
import de.pach.controller.UserRole;
import de.pach.entities.Login;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import static org.mockito.Matchers.anyString;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginModelTest {
    
     @Mock
    Login login;

    @Mock
    LoginController loginController;
    
    @InjectMocks
    LoginModel loginModel;
    
    public LoginModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of login method, of class LoginModel.
     */
    @Test
    public void testLogin() {     
        System.out.println("login");
        
        when(loginController.loginRoleSpecific(anyString(), anyString(), Matchers.any(), Matchers.any())).thenReturn(true);
        
        assertFalse(loginModel.login());
        
        loginModel.setUsername("max.mustermann@mail.de");
        assertFalse(loginModel.login());
        
        loginModel.setPassword("password567");
        assertFalse(loginModel.login());
        
        loginModel.setUserRole(UserRole.DOCTOR);
        assertTrue(loginModel.login());
    }

    /**
     * Test of getUsername method, of class LoginModel.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        LoginModel instance = loginModel;
        String expResult = "mustermann@mail.de";
        instance.setUsername(expResult);
        String result = instance.getUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserRole method, of class LoginModel.
     */
    @Test
    public void testGetUserRole() {
        System.out.println("getUserRole");
        LoginModel instance = loginModel;
        UserRole expResult = UserRole.DOCTOR;
        instance.setUserRole(expResult);
        UserRole result = instance.getUserRole();
        assertEquals(expResult, result);
    }
    
}
