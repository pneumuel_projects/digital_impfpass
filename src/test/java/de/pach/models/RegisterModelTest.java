/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.controller.RegisterController;
import de.pach.entities.profile.EGender;
import de.pach.entities.profile.ETitle;
import de.pach.models.RegisterModel.Usertype;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@RunWith(MockitoJUnitRunner.class)
public class RegisterModelTest {

    @Mock
    RegisterController registerController;
    
    @InjectMocks
    RegisterModel registerModel;

    public RegisterModelTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        registerModel.init();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setUsertype method, of class RegisterModel.
     */
    @Test
    public void testSetGetUsertype() {
        System.out.println("setUsertype");
        RegisterModel.Usertype usertype = Usertype.DOCTOR;
        RegisterModel instance = registerModel;
        System.out.println("setUsertype");
        instance.setUsertype(usertype);
        assertEquals(usertype, instance.getUsertype());
    }

    /**
     * Test of getUsername method, of class RegisterModel.
     */
    @Test
    public void testSetGetUsername() {
        System.out.println("setUsername");
        RegisterModel instance = registerModel;
        String expResult = "Max";
        instance.setUsername(expResult);
        System.out.println("setUsername");
        String result = instance.getUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLastname method, of class RegisterModel.
     */
    @Test
    public void testSetGetLastname() {
        System.out.println("setLastname");
        String lastname = "Mustermann";
        RegisterModel instance = registerModel;
        instance.setLastname(lastname);
        System.out.println("getLastname");
        String result = instance.getLastname();
        assertEquals(lastname, result);
    }

    /**
     * Test of setTitle method, of class RegisterModel.
     */
    @Test
    public void testSetGetTitle() {
        System.out.println("setTitle");
        ETitle title = ETitle.DRPROF;
        RegisterModel instance = registerModel;
        instance.setTitle(title);
        System.out.println("getTitle");
        ETitle result = instance.getTitle();
        assertEquals(title, result);
    }

    /**
     * Test of setGender method, of class RegisterModel.
     */
    @Test
    public void testSetGetGender() {
        System.out.println("setGender");
        EGender gender = EGender.MALE;
        RegisterModel instance = registerModel;
        instance.setGender(gender);
        System.out.println("getGender");
        EGender result = instance.getGender();
        assertEquals(gender, result);
    }

    /**
     * Test of setEmail method, of class RegisterModel.
     */
    @Test
    public void testSetGetEmail() {
        System.out.println("setEmail");
        String email = "max.mustermanm@mail.de";
        RegisterModel instance = registerModel;
        instance.setEmail(email);
        System.out.println("getEmail");
        String result = instance.getEmail();
        assertEquals(email, result);
    }

    /**
     * Test of setPassword method, of class RegisterModel.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "Musterpasswort543!&";
        RegisterModel instance = registerModel;
        instance.setPassword(password);
    }

    /**
     * Test of setStreetname method, of class RegisterModel.
     */
    @Test
    public void testSetGetStreetname() {
        System.out.println("setStreetname");
        String streetname = "Musterstraße";
        RegisterModel instance = registerModel;
        instance.setStreetname(streetname);
        System.out.println("getStreetname");
        String result = instance.getStreetname();
        assertEquals(streetname, result);
    }

    /**
     * Test of setHousenumber method, of class RegisterModel.
     */
    @Test
    public void testSetGetHousenumber() {
        System.out.println("setHousenumber");
        String housenumber = "42";
        RegisterModel instance = registerModel;
        instance.setHousenumber(housenumber);
        System.out.println("getHousenumber");
        String result = instance.getHousenumber();
        assertEquals(housenumber, result);
    }

    /**
     * Test of setCityname method, of class RegisterModel.
     */
    @Test
    public void testSetGetCityname() {
        System.out.println("setCityname");
        String cityName = "Musterstadt";
        RegisterModel instance = registerModel;
        instance.setCityName(cityName);
        System.out.println("getCityname");
        String result = instance.getCityName();
        assertEquals(cityName, result);
    }

    /**
     * Test of setZipcode method, of class RegisterModel.
     */
    @Test
    public void testSetGetZipcode() {
        System.out.println("setZipcode");
        String zipcode = "42425";
        RegisterModel instance = registerModel;
        instance.setZipcode(zipcode);
        System.out.println("getZipcode");
        String result = instance.getZipcode();
        assertEquals(zipcode, result);
    }

    /**
     * Test of setCountry method, of class RegisterModel.
     */
    @Test
    public void testSetCountry() {
        System.out.println("setCountry");
        String country = "Musterland";
        RegisterModel instance = registerModel;
        instance.setCountry(country);
        System.out.println("getCountry");
        String result = instance.getCountry();
        assertEquals(country, result);
    }

    /**
     * Test of finalizeRegistration method, of class RegisterModel.
     */
    @Test
    public void testFinalizeRegistration() {
        System.out.println("finalizeRegistration");
        RegisterModel instance = registerModel;
        instance.setUsertype(null);
        try {
            instance.finalizeRegistration();
            fail("No Exception thrown despite Usertype = null");
        } catch (NullPointerException e) {

        }

        instance.setUsertype(Usertype.DOCTOR);
        instance.finalizeRegistration();

        instance.setUsertype(Usertype.PATIENT);
        instance.finalizeRegistration();
    }

    /**
     * Test of validateUser method, of class RegisterModel.
     */
    @Test
    public void testValidateUser() {
        when(registerController.verifyUser(any(UUID.class))).thenReturn(true);
        
        System.out.println("validateUser");
        String s_uuid = UUID.randomUUID().toString();
        RegisterModel instance = registerModel;
        boolean expResult = true;
        boolean result = instance.validateUser(s_uuid);
        assertEquals(expResult, result);
    }

}
