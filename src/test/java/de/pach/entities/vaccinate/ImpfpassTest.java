/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.vaccinate;


import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class ImpfpassTest {
    
    public ImpfpassTest() {
    }

    /**
     * Test of toString method, of class Impfpass.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Impfpass instance = new Impfpass();
        String expResult = "de.pach.entities.impfen.Impfpass" + " id: " + instance.getId();
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of getImpfungen method, of class Impfpass.
     */
    @Test
    public void testListImpfungen() {
    
        Impfpass instatance = new Impfpass();
        
        assertNotEquals(null, instatance.getImpfungen());
        
        Impfpass insta = new Impfpass();
        
        
        Impfung impfungen1 = new Impfung();
        Impfstoff impfstoff1 = new Impfstoff();
        impfstoff1.setBeschreibung("1");
        impfungen1.setImpstoff(impfstoff1);
        
        Impfung impfungen2 = new Impfung();
        Impfstoff impfstoff2 = new Impfstoff();
        impfstoff2.setBeschreibung("2");
        impfungen2.setImpstoff(impfstoff2);
        
        Impfung impfungen3 = new Impfung();
        Impfstoff impfstoff3 = new Impfstoff();
        impfstoff3.setBeschreibung("3");
        impfungen3.setImpstoff(impfstoff3);
        
        List<Impfung> list = new ArrayList<>();
        list.add(impfungen1);
        list.add(impfungen2);
        
        insta.setImpfungen(list);
        instatance.setImpfungen(list);

        assertTrue(list.get(0).getImpstoff().getBeschreibung().equals(impfstoff1.getBeschreibung()));
        assertFalse(list.get(0).getImpstoff().getBeschreibung().equals(impfstoff2.getBeschreibung()));
        
        assertEquals(instatance.getImpfungen().size(),insta.getImpfungen().size());
        /*
        //assertFalse(instatance.getImpfungen().contains(impfungen1));
        assertFalse(instatance.getImpfungen().contains(impfungen2));
        //assertFalse(list.size() == 2);
        assertTrue(instatance.getImpfungen().contains(impfungen3.getImpstoff()));*/
    }
}
