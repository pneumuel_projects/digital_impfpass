/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.vaccinate;

import java.time.LocalDate;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class ImpfungenTest {
    
    public ImpfungenTest() {
    }
    

    /**
     * Test of toString method, of class Impfung.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Impfung instance = new Impfung();
        String expResult = "de.pach.entities.impfen.Impfungen" + " id: " + instance.getId();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getImpfDate method, of class Impfung.
     */
    @Test
    public void testGetImpfDate(){
        Impfung impfungen = new Impfung();
        impfungen.setImpfDate(LocalDate.MIN);
        assertTrue(LocalDate.MIN.equals(impfungen.getImpfDate()));


    }

    /**
     * Test of setImpfDate method, of class Impfung.
     */
    @Test
    public void testSetImpfDate() {
        Impfung impfungen = new Impfung();
        impfungen.setImpfDate(LocalDate.MIN);
        assertTrue(LocalDate.MIN.equals(impfungen.getImpfDate()));
    }

    /**
     * Test of getImpstoff method, of class Impfung.
     */
    @Test
    public void testGetImpstoff() {
        Impfung instance = new Impfung();
        Impfstoff impf = new Impfstoff();
        impf.setImpfstoffart(EImpfstoffart.MONO);
        instance.setImpstoff(impf);
        
        Impfstoff impfstoff = new Impfstoff();
        
        impfstoff.setImpfstoffart(EImpfstoffart.MONO);
        
        
        assertEquals(impfstoff.getImpfstoffart(),
                instance.getImpstoff().getImpfstoffart());
        
    }

    /**
     * Test of setImpstoff method, of class Impfung.
     */
    @Test
    public void testSetImpstoff() {
        System.out.println("setImpstoff");
        Impfstoff impstoff = new Impfstoff();
        
        Impfung instance = new Impfung();
        instance.setImpstoff(impstoff);
        
        assertEquals(impstoff, instance.getImpstoff());
    }
    
}
