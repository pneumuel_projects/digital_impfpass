/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.vaccinate;

import java.time.LocalDate;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class ImpfstoffTest {

    public ImpfstoffTest() {
    }

    /**
     * Test of toString method, of class Impfstoff.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Impfstoff instance = new Impfstoff();
        String expResult = "de.pach.entities.impfstoff.Impfstoff" + " id: " + instance.getId();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBeschreibung method, of class Impfstoff.
     */
    @Test
    public void testGetBeschreibung() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setBeschreibung("foo");
        assertTrue("foo".equals(impfstoff.getBeschreibung()));
    }

    /**
     * Test of setBeschreibung method, of class Impfstoff.
     */
    @Test
    public void testSetBeschreibung() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setBeschreibung("foo");
        assertTrue("foo".equals(impfstoff.getBeschreibung()));
    }

    /**
     * Test of getIndikationsgruppe method, of class Impfstoff.
     */
    @Test
    public void testGetIndikationsgruppe() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setIndikationsgruppe("bar");
        assertTrue("bar".equals(impfstoff.getIndikationsgruppe()));
    }

    /**
     * Test of setIndikationsgruppe method, of class Impfstoff.
     */
    @Test
    public void testSetIndikationsgruppe() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setIndikationsgruppe("bar");
        assertTrue("bar".equals(impfstoff.getIndikationsgruppe()));
    }

    /**
     * Test of getZulassungsInhaber method, of class Impfstoff.
     */
    @Test
    public void testGetZulassungsInhaber() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setZulassungsInhaber("EvilCorp");
        assertTrue("EvilCorp".equals(impfstoff.getZulassungsInhaber()));
    }

    /**
     * Test of setZulassungsInhaber method, of class Impfstoff.
     */
    @Test
    public void testSetZulassungsInhaber() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setZulassungsInhaber("EvilCorp");
        assertTrue("EvilCorp".equals(impfstoff.getZulassungsInhaber()));
    }

    /**
     * Test of getImpfstoffart method, of class Impfstoff.
     */
    @Test
    public void testGetImpfstoffart() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setImpfstoffart(EImpfstoffart.MONO);
        assertTrue(EImpfstoffart.MONO.equals(impfstoff.getImpfstoffart()));
        assertFalse(EImpfstoffart.KOMBI.equals(impfstoff.getImpfstoffart()));
    }

    /**
     * Test of setImpfstoffart method, of class Impfstoff.
     */
    @Test
    public void testSetImpfstoffart() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setImpfstoffart(EImpfstoffart.MONO);
        assertTrue(EImpfstoffart.MONO.equals(impfstoff.getImpfstoffart()));
        assertFalse(EImpfstoffart.KOMBI.equals(impfstoff.getImpfstoffart()));
    }

    /**
     * Test of getZulassungsnummer method, of class Impfstoff.
     */
    @Test
    public void testGetZulassungsnummer() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setZulassungsnummer("123456");
        assertTrue("123456".equals(impfstoff.getZulassungsnummer()));
    }

    /**
     * Test of setZulassungsnummer method, of class Impfstoff.
     */
    @Test
    public void testSetZulassungsnummer() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setZulassungsnummer("123456");
        assertTrue("123456".equals(impfstoff.getZulassungsnummer()));
    }

    /**
     * Test of getZulassungsDatum method, of class Impfstoff.
     */
    @Test
    public void testGetZulassungsDatum() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setZulassungsDatum(LocalDate.MIN);
        assertTrue(LocalDate.MIN.equals(impfstoff.getZulassungsDatum()));
        assertFalse(LocalDate.MAX.equals(impfstoff.getZulassungsDatum()));
    }

    /**
     * Test of setZulassungsDatum method, of class Impfstoff.
     */
    @Test
    public void testSetZulassungsDatum() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setZulassungsDatum(LocalDate.MIN);
        assertTrue(LocalDate.MIN.equals(impfstoff.getZulassungsDatum()));
        assertFalse(LocalDate.MAX.equals(impfstoff.getZulassungsDatum()));
    }

    /**
     * Test of getInformationen method, of class Impfstoff.
     */
    @Test
    public void testGetInformationen() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setInformationen("blub");
        assertTrue("blub".equals(impfstoff.getInformationen()));

    }

    /**
     * Test of setInformationen method, of class Impfstoff.
     */
    @Test
    public void testSetInformationen() {
        Impfstoff impfstoff = new Impfstoff();
        impfstoff.setInformationen("blub");
        assertTrue("blub".equals(impfstoff.getInformationen()));
    }

}
