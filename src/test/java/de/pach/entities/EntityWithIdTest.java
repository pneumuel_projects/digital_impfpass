/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class EntityWithIdTest {

    public EntityWithIdTest() {
    }

    /**
     * Test of getId method, of class EntityWithId.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        EntityWithId instance = new EntityWithIdImpl();
        Long expResult = 0L;
        Long result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class EntityWithId.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = 5L;
        EntityWithId instance = new EntityWithIdImpl();
        instance.setId(id);
        assertEquals(id, instance.getId());
    }

    /**
     * Test of hashCode method, of class EntityWithId.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        EntityWithId instance = new EntityWithIdImpl();
        int expResult = instance.getId().hashCode();
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class EntityWithId.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        EntityWithId otherInstance = new EntityWithIdImpl();
        otherInstance.setId(5L);
        EntityWithId instance = new EntityWithIdImpl();
        Object noInstance = new Object();
        boolean resultDiff = instance.equals(otherInstance);
        boolean resultSame = instance.equals(instance);
        boolean resultDifferentType = instance.equals(noInstance);
        assertEquals(false, resultDiff);
        assertEquals(true, resultSame);
        assertEquals(false, resultDifferentType);
    }

    /**
     * Test of toString method, of class EntityWithId.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        EntityWithId instance = new EntityWithIdImpl();
        String expResult = "de.pach.entities.EntityWithId[ id=" + instance.getId() + " ]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    public class EntityWithIdImpl extends EntityWithId{}
}
