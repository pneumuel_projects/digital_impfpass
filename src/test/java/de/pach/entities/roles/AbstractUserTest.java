/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.roles;

import de.pach.entities.profile.Profile;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author chriss
 */
public class AbstractUserTest {
    
    public AbstractUserTest() {
    }

    /**
     * Test of getUserProfile method, of class AbstractUser.
     */
    @Test
    public void testGetUserProfile() {
        System.out.println("getUserProfile");
        AbstractUser instance = new AbstractUserImpl();
        Profile expResult = null;
        Profile result = instance.getUserProfile();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUserProfile method, of class AbstractUser.
     */
    @Test
    public void testSetUserProfile() {
        System.out.println("setUserProfile");
        Profile userProfile = new Profile();
        AbstractUser instance = new AbstractUserImpl();
        instance.setUserProfile(userProfile);
        assertEquals(instance.getUserProfile(), userProfile);
    }

    public class AbstractUserImpl extends AbstractUser {
    }
    
}
