/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.roles;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class DoctorTest {
    
    public DoctorTest() {
    }
    
    /**
     * Test of toString method, of class Doctor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Doctor instance = new Doctor();
        String expResult = "de.pach.entities.roles.Doctor" + " id: " + instance.getId();
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of getPatients method, of class Doctor.
     */
    /*@Test
    public void testGetPatients() {

        Doctor instance = new Doctor();
        List<Patient> expResult = null;
        List<Patient> result = instance.getPatients();
        assertNotEquals(expResult, result);
        assertEquals(result.size(), 0);
    }*/

    /**
     * Test of setPatients method, of class Doctor.
     */
    /*@Test
    public void testSetPatients() {

        List<Patient> patient = new ArrayList<>();
        patient.add(new Patient());
        Doctor instance = new Doctor();
        instance.setPatients(patient);
        assertEquals(patient.size(), instance.getPatients().size());
    }*/
    
}
