/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.roles;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class AdministratorTest {
    
    public AdministratorTest() {
    }

    /**
     * Test of toString method, of class Administrator.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Administrator instance = new Administrator();
        String expResult = "de.pach.entities.roles.Admin" + " id: " + instance.getId();
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
