/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.roles;

import de.pach.entities.vaccinate.Impfpass;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class PatientTest {
    
    public PatientTest() {
    }

    /**
     * Test of toString method, of class Patient.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Patient instance = new Patient();
        String expResult = "de.pach.entities.roles.Patient" + " id: " + instance.getId();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserImpfpass method, of class Patient.
     */
    @Test
    public void testGetUserImpfpass() {
        System.out.println("getUserImpfpass");
        Patient instance = new Patient();
        Impfpass unexpResult = null;
        Impfpass result = instance.getUserImpfpass();
        assertNotEquals(unexpResult, result);
    }

    /**
     * Test of setUserImpfpass method, of class Patient.
     */
    @Test
    public void testSetUserImpfpass() {
        System.out.println("setUserImpfpass");
        Impfpass userImpfpass = new Impfpass();
        userImpfpass.setId(123L);
        Patient instance = new Patient();
        instance.setUserImpfpass(userImpfpass);
        assertEquals(instance.getUserImpfpass().getId(), userImpfpass.getId());
    }

    /**
     * Test of getAllowedDoctors method, of class Patient.
     */
    @Test
    public void testGetAllowedDoctors() {
        System.out.println("getAllowedDoctors");
        Patient instance = new Patient();
        List<Doctor> expResult = null;
        List<Doctor> result = instance.getAllowedDoctors();
        assertNotEquals(expResult, result);
        assertEquals(0, result.size());
    }

    /**
     * Test of setAllowedDoctors method, of class Patient.
     */
    @Test
    public void testSetAllowedDoctors() {
        System.out.println("setAllowedDoctors");
        List<Doctor> allowedDoctors = new ArrayList<>();
        allowedDoctors.add(new Doctor());
        Patient instance = new Patient();
        instance.setAllowedDoctors(allowedDoctors);
        assertEquals(allowedDoctors.size(), instance.getAllowedDoctors().size());
    }
    
}
