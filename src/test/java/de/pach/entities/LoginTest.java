/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities;

import de.pach.entities.roles.AbstractUser;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class LoginTest {
    
    public LoginTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUser method, of class Login.
     */
    @org.junit.Test
    public void testGetUser() {
        System.out.println("getUser");
        Login instance = new Login();
        AbstractUser expResult = null;
        AbstractUser result = instance.getUser();
        assertEquals(expResult, result);
        // TODO Den Test um einen Testfall ergänzen, in dem ein Nutzer zurückgegeben wird.
    }

    /**
     * Test of setUser method, of class Login.
     */
    @org.junit.Test
    public void testSetUser() {
        System.out.println("setUser");
        AbstractUser user = null;
        Login instance = new Login();
        instance.setUser(user);
        // TODO Den Test erweitern, damit ein User gesetzt wird und das Setzen überprüft wird.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getLastLoginAttempt method, of class Login.
     */
    @org.junit.Test
    public void testGetLastLoginAttempt() {
        System.out.println("getLastLoginAttempt");
        Login instance = new Login();
        LocalDateTime expResult = (Timestamp.valueOf(LocalDateTime.MIN)).toLocalDateTime();
        LocalDateTime result = instance.getLastLoginAttempt();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLastLoginAttempt method, of class Login.
     */
    @org.junit.Test
    public void testSetLastLoginAttempt() {
        System.out.println("setLastLoginAttempt");
        LocalDateTime lastLogin = LocalDateTime.now();
        Login instance = new Login();
        instance.setLastLoginAttempt(lastLogin);
        assertEquals(lastLogin, instance.getLastLoginAttempt());
    }

    /**
     * Test of getFailedLoginAttempts method, of class Login.
     */
    @org.junit.Test
    public void testGetFailedLoginAttempts() {
        System.out.println("getFailedLoginAttempts");
        Login instance = new Login();
        int expResult = 0;
        int result = instance.getFailedLoginAttempts();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFailedLoginAttempts method, of class Login.
     */
    @org.junit.Test
    public void testSetFailedLoginAttempts() {
        System.out.println("setFailedLoginAttempts");
        int failedLoginAttempts = 20;
        Login instance = new Login();
        instance.setFailedLoginAttempts(failedLoginAttempts);
        assertEquals(instance.getFailedLoginAttempts(), failedLoginAttempts);
    }

    /**
     * Test of isLoggedIn method, of class Login.
     */
    @org.junit.Test
    public void testIsLoggedIn() {
        System.out.println("isLoggedIn");
        Login instance = new Login();
        boolean expResult = false;
        boolean result = instance.isLoggedIn();
        assertEquals(expResult, result);
        // TODO Test erweitern, damit auch TRUE getestet werden kann.
    }

    /**
     * Test of increaseFailedLoginAttempts method, of class Login.
     */
    @org.junit.Test
    public void testIncreaseFailedLoginAttempts() {
        System.out.println("increaseFailedLoginAttempts");
        Login instance = new Login();
        int failedLoginAttempts = 5;
        instance.setFailedLoginAttempts(failedLoginAttempts);
        instance.increaseFailedLoginAttempts();
        assertEquals(instance.getFailedLoginAttempts(), failedLoginAttempts + 1);
    }

    /**
     * Test of toString method, of class Login.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Login instance = new Login();
        String expResult = "de.pach.entities.Login[ id=" + instance.getId() + " ]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
