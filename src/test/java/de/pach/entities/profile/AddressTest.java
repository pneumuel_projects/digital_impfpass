/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.profile;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class AddressTest {
    
    public AddressTest() {
    }

    /**
     * Test of toString method, of class Address.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Address instance = new Address();
        String expResult = "de.pach.entities.profile.Address";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getZipCode method, of class Address.
     */
    @Test
    public void testGetZipCode() {
        System.out.println("getZipCode");
        Address instance = new Address();
        String expResult = "00000";
        String result = instance.getZipCode();
        assertEquals(expResult, result); 
    }

    /**
     * Test of setZipCode method, of class Address.
     */
    @Test
    public void testSetZipCode() {
        System.out.println("setZipCode");
        String zipCode = "12345";
        Address instance = new Address();
        instance.setZipCode(zipCode);
        assertEquals(zipCode, instance.getZipCode());
    }

    /**
     * Test of getStreetname method, of class Address.
     */
    @Test
    public void testGetStreetname() {
        System.out.println("getStreetname");
        Address instance = new Address();
        String expResult = "unknown";
        String result = instance.getStreetname();
        assertEquals(expResult, result);
    }

    /**
     * Test of setStreetname method, of class Address.
     */
    @Test
    public void testSetStreetname() {
        System.out.println("setStreetname");
        String streetname = "elmestreet";
        Address instance = new Address();
        instance.setStreetname(streetname);
        assertEquals(streetname, instance.getStreetname());
    }

    /**
     * Test of getHousenumber method, of class Address.
     */
    @Test
    public void testGetHousenumber() {
        System.out.println("getHousenumber");
        Address instance = new Address();
        String expResult = "0000";
        String result = instance.getHousenumber();
        assertEquals(expResult, result);
    }

    /**
     * Test of setHousenumber method, of class Address.
     */
    @Test
    public void testSetHousenumber() {
        System.out.println("setHousenumber");
        String housenumber = "34";
        Address instance = new Address();
        instance.setHousenumber(housenumber);
        assertEquals(housenumber, instance.getHousenumber());
    }

    /**
     * Test of getCityname method, of class Address.
     */
    @Test
    public void testGetCityname() {
        System.out.println("getCityname");
        Address instance = new Address();
        String expResult = "unknown";
        String result = instance.getCityname();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCityname method, of class Address.
     */
    @Test
    public void testSetCityname() {
        System.out.println("setCityname");
        String cityname = "Osna";
        Address instance = new Address();
        instance.setCityname(cityname);
        assertEquals(cityname, instance.getCityname());
    }

    /**
     * Test of getCountry method, of class Address.
     */
    @Test
    public void testGetCountry() {
        System.out.println("getCountry");
        Address instance = new Address();
        String expResult = "unknown";
        String result = instance.getCountry();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCountry method, of class Address.
     */
    @Test
    public void testSetCountry() {
        System.out.println("setCountry");
        String country = "de";
        Address instance = new Address();
        instance.setCountry(country);
        assertEquals(country, instance.getCountry());
    }
    
}
