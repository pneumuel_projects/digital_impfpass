/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.profile;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class UserLoginTest {
    
    public UserLoginTest() {
    }

    /**
     * Test of hashPassword method, of class UserLogin.
     */
    @Test
    public void testHashPassword() {
        System.out.println("hashPassword");
        String password = "password";
        UserLogin instance = new UserLogin();
        
        //Passwort zwei mal mit der selben instanz Hashen.
        byte[] expResult = instance.hashPassword(password);
        byte[] result = instance.hashPassword(password);
        
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getEmail method, of class UserLogin.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        UserLogin instance = new UserLogin();
        String expResult = null;
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmail method, of class UserLogin.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "test@mail.de";
        UserLogin instance = new UserLogin();
        instance.setEmail(email);
        assertEquals(email, instance.getEmail());
    }

    /**
     * Test of getPassword method, of class UserLogin.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        UserLogin instance = new UserLogin();
        byte[] expResult = null;
        byte[] result = instance.getPassword();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of loginCorrect method, of class UserLogin.
     */
    @Test
    public void testLoginCorrect() {
        System.out.println("loginCorrect");
        
         UserLogin instance = new UserLogin();
        String password = "password";
        instance.setPassword(password);
        boolean result = instance.loginCorrect(password);
        assertEquals(true, result);
        result = instance.loginCorrect("notthepassword");
        assertEquals(false, result);
    }

    /**
     * Test of setPassword method, of class UserLogin.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
       
        UserLogin instance = new UserLogin();
        String password = "password";
        byte[] expResult = instance.hashPassword(password);
        instance.setPassword(password);
        byte[] result = instance.getPassword();
        assertArrayEquals(expResult, result);
    }
    
}
