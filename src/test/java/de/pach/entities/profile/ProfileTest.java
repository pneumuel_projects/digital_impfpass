/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.profile;

import java.util.UUID;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public class ProfileTest {
    
    public ProfileTest() {
    }

    /**
     * Test of toString method, of class Profile.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Profile instance = new Profile();
        String expResult = "de.pach.entities.profile.Profile id: " + instance.getId();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUserLogin method, of class Profile.
     */
    @Test
    public void testGetUserLogin() {
        System.out.println("getUserLogin");
        Profile instance = new Profile();
        UserLogin expResult = null;
        UserLogin result = instance.getUserLogin();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of setUserLogin method, of class Profile.
     */
    @Test
    public void testSetUserLogin() {
        System.out.println("setUserLogin");
        UserLogin userLogin = new UserLogin();
        userLogin.setId(123L);
        Profile instance = new Profile();
        instance.setUserLogin(userLogin);
        assertEquals(instance.getUserLogin().getId(), userLogin.getId());
    }

    /**
     * Test of getName method, of class Profile.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Profile instance = new Profile();
        String expResult = "unknown";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class Profile.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "TestProfile";
        Profile instance = new Profile();
        instance.setName(name);
        assertEquals(name, instance.getName());
    }

    /**
     * Test of getLastname method, of class Profile.
     */
    @Test
    public void testGetLastname() {
        System.out.println("getLastname");
        Profile instance = new Profile();
        String expResult = "unknown";
        String result = instance.getLastname();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLastname method, of class Profile.
     */
    @Test
    public void testSetLastname() {
        System.out.println("setLastname");
        String lastname = "TestLastName";
        Profile instance = new Profile();
        instance.setLastname(lastname);
        assertEquals(instance.getLastname(), lastname);
    }

    /**
     * Test of getTitle method, of class Profile.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Profile instance = new Profile();
        ETitle expResult = ETitle.NOTHING;
        ETitle result = instance.getTitle();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTitle method, of class Profile.
     */
    @Test
    public void testSetTitle() {
        System.out.println("setTitle");
        ETitle title = ETitle.DRPROF;
        Profile instance = new Profile();
        instance.setTitle(title);
        assertEquals(instance.getTitle(), title);
    }

    /**
     * Test of getGender method, of class Profile.
     */
    @Test
    public void testGetGender() {
        System.out.println("getGender");
        Profile instance = new Profile();
        EGender expResult = EGender.UNKOWN;
        EGender result = instance.getGender();
        assertEquals(expResult, result);
    }

    /**
     * Test of setGender method, of class Profile.
     */
    @Test
    public void testSetGender() {
        System.out.println("setGender");
        EGender gender = EGender.MALE;
        Profile instance = new Profile();
        instance.setGender(gender);
        assertEquals(gender, instance.getGender());
    }

    /**
     * Test of isVerified method, of class Profile.
     */
    @Test
    public void testIsVerified() {
        System.out.println("isVerified");
        Profile instance = new Profile();
        boolean expResult = false;
        boolean result = instance.isVerified();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVerified method, of class Profile.
     */
    @Test
    public void testSetVerified() {
        System.out.println("setVerified");
        boolean verified = true;
        Profile instance = new Profile();
        instance.setVerified(verified);
        assertEquals(verified, instance.isVerified());
    }

    /**
     * Test of getUserUUID method, of class Profile.
     */
    @Test
    public void testGetUserUUID() {
        System.out.println("getUserUUID");
        Profile instance = new Profile();
        UUID unexpResult = null;
        UUID result = instance.getUserUUID();
        assertNotEquals(unexpResult, result);
    }

    /**
     * Test of setUserUUID method, of class Profile.
     */
    @Test
    public void testSetUserUUID() {
        System.out.println("setUserUUID");
        UUID uuid = UUID.randomUUID();
        Profile instance = new Profile();
        instance.setUserUUID(uuid);
        assertEquals(uuid, instance.getUserUUID());
    }
    
}
