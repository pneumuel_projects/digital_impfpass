/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.util.comparators;

import de.pach.entities.vaccinate.Impfung;
import java.util.Comparator;

/**
 *
 * @author chriss
 */
public class VaccineDateComparator implements Comparator {

    @Override
    public int compare(Object t, Object t1) {
        if(t.getClass() != Impfung.class || t1.getClass() != Impfung.class)
        {
            throw new IllegalArgumentException("Die zu vergleichenden Objekte sind nicht vom Typ Impfung! (" + Impfung.class + ": " + t.getClass() + ", " + t1.getClass() +")");
        }
        if(((Impfung)t).getImpfDate().isBefore(((Impfung)t1).getImpfDate()))
        {
            return 1;
        }else if(((Impfung)t1).getImpfDate().isBefore(((Impfung)t).getImpfDate()))
        {
            return -1;
        }
        return 0;
    }
    
}
