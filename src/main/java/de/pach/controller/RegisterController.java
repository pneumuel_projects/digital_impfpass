/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.controller;

import de.pach.entities.roles.AbstractUser;
import de.pach.repositories.UserRepo;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author chriss
 */
@Stateless
public class RegisterController {

    @Inject
    UserRepo userRepository;

    public boolean finishRegistration(AbstractUser user) {
        if (userRepository.getUserByEMAIL(user.getClass(), user.getUserProfile().getUserLogin().getEmail()) == null) {
            userRepository.addUser(user);
            return true;
        }
        return false;
    }

    public boolean verifyUser(UUID uuid) {
        AbstractUser u = userRepository.getUserByUUID(AbstractUser.class, uuid);
        if (u == null) {
            return false;
        }

        u.getUserProfile().setVerified(true);
        userRepository.updateUser(u);
        return true;
    }

}
