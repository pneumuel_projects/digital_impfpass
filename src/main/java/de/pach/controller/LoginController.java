/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.controller;

import static de.pach.controller.UserRole.UNSPECIFIED;
import de.pach.entities.Login;
import de.pach.entities.roles.AbstractUser;
import de.pach.entities.roles.Doctor;
import de.pach.entities.roles.Patient;
import de.pach.repositories.LoginCrud;
import de.pach.repositories.UserRepo;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import javax.inject.Inject;

/**
 *
 * @author chriss
 */
public class LoginController {

    @Inject
    UserRepo userRepository;

    @Inject
    LoginCrud loginCrud;

    //Diese Methode prüft ob das Login zu einem Nutzer der gegebenen UserRole passt.
    //Ist das Login gültig für einen Doktor aber die UserRole ist Patient, ist es trotzdem fehlgeschlagen.
    //Außerdem muss die gegebene Rolle eine Spezialisierung des User sein (Doctor oder Patient)
    public boolean loginRoleSpecific(String username, String password, UserRole userRole, Login login) {
        if (userRole == null || userRole == UNSPECIFIED) {
            return false;
        }
        return login(username, password, userRole, login);
    }

    //Diese Methode prüft ob das Login zu einem Nutzer der gegebenen UserRole passt.
    //Ist das Login gültig für einen Doktor aber die UserRole ist Patient, ist es trotzdem fehlgeschlagen.
    //Das kann (sollte aber nicht) umgehen werden, in dem die UserRole auf UNSPECIFIED gesetzt wird.
    //
    private boolean login(String username, String password, UserRole userRole, Login login) {
        //Zeitspanne zum letzten Login berechnen
        long minutesSinceLastLogin = login.getLastLoginAttempt().until(LocalDateTime.now(), ChronoUnit.MINUTES);

        //Prüfen ob die maximale Anzahl an Loginversuchen erreicht wurde.
        if (login.getFailedLoginAttempts() >= 3 && minutesSinceLastLogin <= 5) {
            //Login ist momentan gesperrt..
            login.increaseFailedLoginAttempts();
            login.setLastLoginAttempt(LocalDateTime.now());
            login.setUser(null);
            return false;
        }

        //Zeit des aktuellen Loginversuches festhalten
        login.setLastLoginAttempt(LocalDateTime.now());

        //Das passende Nutzerprofil laden, falls noch nicht geschehen
        Class t;
        switch (userRole) {
            case DOCTOR:
                t = Doctor.class;
                break;
            case PATIENT:
                t = Patient.class;
                break;
            default:
                t = AbstractUser.class;

        }
        AbstractUser user = userRepository.getUserByEMAIL(t, username);

        //Wenn kein Nutzer mit der Email existiert, Login fehlgeschlagen
        if (user == null) {
            login.setUser(null);
            login.increaseFailedLoginAttempts();
            login.setLastLoginAttempt(LocalDateTime.now());
            return false;
        }

        //Passwort überprüfen
        if (user.getUserProfile().getUserLogin().loginCorrect(password) && user.getUserProfile().isVerified()) {
            //Login korrekt
            login.setUser(user);
            login.setFailedLoginAttempts(0);

            

            return true;
        } else {
            login.setUser(null);
            login.increaseFailedLoginAttempts();
            login.setLastLoginAttempt(LocalDateTime.now());
        }
        return false;
    }
    
    public Login reLoginUser(Login login){
        if (login.getId() == null || loginCrud.getEntity(login.getId()) == null) {
                return loginCrud.create(login);
            } else {
                return loginCrud.update(login);

            }
    }

    public Login logoutUser(Login login) {
        login.setUser(null);
        try {
            return loginCrud.update(login);
        } catch (Exception e) {

        }
        return null;
    }
}
