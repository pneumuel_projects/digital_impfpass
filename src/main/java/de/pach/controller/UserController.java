/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.controller;

import de.pach.entities.roles.AbstractUser;
import de.pach.repositories.UserRepo;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author chriss
 */
@Stateless
public class UserController{
     @Inject
     UserRepo userRepo;
     
     public AbstractUser updateUser(AbstractUser user)
     {
         return userRepo.updateUser(user);
     }
     
}
