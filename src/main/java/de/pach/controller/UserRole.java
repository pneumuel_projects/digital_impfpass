/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.controller;

/**
 *
 * @author chriss
 */
public enum UserRole {
    UNSPECIFIED,
    PATIENT,
    DOCTOR
}
