package de.pach.crawler;

import de.pach.entities.vaccinate.EImpfstoffart;
import de.pach.entities.vaccinate.Impfstoff;
import de.pach.repositories.ImpfstoffRepo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Startup;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class Crawler {

    @Inject
    ImpfstoffRepo impfstoffRepo;

    private AtomicBoolean busy = new AtomicBoolean(false);
    
    @PostConstruct
    public void manualRun()
    {
        try{
        mining();
        }catch(Exception e)
        {
        
        }
    }
    
    //Scheduler alle 24 Stunden aufruf
    @Schedule(hour = "*", minute = "*/5", second = "1", persistent = false)
    public void mining() throws InterruptedException {
        System.out.println(busy.get());
        if (busy.getAndSet(true)) {
            System.out.print("Schedule für Impfstoff-Crawler fehlgeschlagen, Crawler ist busy.");
            return;
        }
        
        System.out.print("Starte Impfstoff-Crawler...");
        try {
            readPage(PageConstant.CHOLERA.toString(), PageConstant.CHOLERA);
            readPage(PageConstant.DIPHTHERIE.toString(), PageConstant.DIPHTHERIE);
        } catch (IOException ex) {
            System.err.println("Fehler beim ausführen des Impfstoff-Crawlers!");
            Logger.getLogger(Crawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.print("Impfstoffe aktualisiert.");
        busy.set(false);
    }

    private void readPage(String url, PageConstant krankheit) throws IOException {

        Document doc = Jsoup.connect(url).get();

        doc.select("table").forEach((table) -> {
            table.select("tr").forEach((row) -> {
                Elements links = doc.select("a[href]");
                Elements tds = row.select("td");
                if (tds.size() > 6) {
                    /*System.out.println("HIER DIE AUSGABE\n_____________________");
                    System.out.println("Bezeichnung: " + tds.get(0).text());
                    System.out.println("Indikationsgruppe: " + tds.get(1).text());
                    System.out.println("Zulassungsinhaber: " + tds.get(2).text());
                    System.out.println("Impfstoffart: " + tds.get(3).text());
                    System.out.println("Zulassungsnummer: " + tds.get(4).text());
                    System.out.println("Zulassungsdatum: " + tds.get(5).text());
                    System.out.println("Informationen: " + tds.get(6).text());*/

                    //Impfstoff find = impfstoffRepo.findByName(tds.get(4).text());

                    if (impfstoffRepo.findByName(tds.get(4).text())== null) {
                       
                        Impfstoff impfstoff = new Impfstoff();

                        impfstoff.setKrankheit(krankheit);
                        
                        switch (krankheit)
                        {
                            case CHOLERA:
                                impfstoff.setDuration(Period.ofYears(2));
                                break;
                            case DIPHTHERIE:
                                impfstoff.setDuration(Period.ofYears(10));
                                break;
                            default:
                                impfstoff.setDuration(Period.ofYears(1));    
                        }

                        impfstoff.setBeschreibung(tds.get(0).text());
                        impfstoff.setIndikationsgruppe(tds.get(1).text());
                        impfstoff.setZulassungsInhaber(tds.get(2).text());
                        switch (tds.get(3).text()) {
                            case "Mono":
                                impfstoff.setImpfstoffart(EImpfstoffart.MONO);
                                break;
                            case "Kombi":
                                impfstoff.setImpfstoffart(EImpfstoffart.KOMBI);
                                break;
                            default:
                                impfstoff.setImpfstoffart(EImpfstoffart.UNKOWN);
                                break;
                        }
                        impfstoff.setZulassungsnummer(tds.get(4).text());

                        String date = tds.get(5).text();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", new Locale("de"));
                        impfstoff.setZulassungsDatum(LocalDate.parse(date, formatter));

                        impfstoff.setInformationen(tds.get(6).text());

                        impfstoffRepo.create(impfstoff);

                    }

                }
            });
        });
    }
}
