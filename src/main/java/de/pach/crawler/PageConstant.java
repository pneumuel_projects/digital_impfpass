/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.crawler;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
public enum PageConstant {
    
    //TODO: Alle Krankheiten hinzufügen..
    
    UNKOWN ("unkown"),
    CHOLERA("https://www.pei.de/DE/arzneimittel/impfstoff-impfstoffe"
            + "-fuer-den-menschen/cholera/cholera-node.html"),
    DIPHTHERIE("https://www.pei.de/DE/arzneimittel/impfstoff"
            + "-impfstoffe-fuer-den-menschen/diphtherie/diphtherie-node.html");
    
    private PageConstant() {
        this.name = null;
    }
    
    private final String name;
    
    private PageConstant(String s){
        name = s;
    }
    
    public boolean equalsName(String otherName) {
        if(otherName != null)
        return name.equals(otherName);
        else{
            return false;
        }
    }
    
    @Override
    public String toString() {
       return this.name;
    }
}
