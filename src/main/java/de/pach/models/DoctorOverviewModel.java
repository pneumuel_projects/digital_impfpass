/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.controller.UserController;
import de.pach.entities.roles.AbstractUser;
import de.pach.entities.roles.Doctor;
import de.pach.entities.roles.Patient;
import de.pach.repositories.UserRepo;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author chriss
 */
@Named
@SessionScoped
public class DoctorOverviewModel implements Serializable {

    @Inject
    LoginSessionWrapper session;

    @Inject
    UserRepo userRepo;
    
    @Inject
    UserController userController;

    private List<Doctor> doctors;

    public List<Doctor> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Doctor> doctors) {
        this.doctors = doctors;
    }

    public List<Doctor> getFilteredDoctors() {
        return filteredDoctors;
    }

    public void setFilteredDoctors(List<Doctor> filteredDoctors) {
        this.filteredDoctors = filteredDoctors;
    }
    private List<Doctor> filteredDoctors;
    
    @PostConstruct
    public void init()
    {
        doctors = userRepo.getAll(Doctor.class);
    }
    
    public List<Doctor> getPatientsDoctors() {
        return ((Patient) session.getLogin().getUser()).getAllowedDoctors();
    }

    public void removeDoctor(Doctor d) {
        ((Patient) session.getLogin().getUser()).removeAllowedDoctor(d);
        AbstractUser p = userController.updateUser(session.getLogin().getUser());
        session.getLogin().setUser(p);
    }

    public void showAddDoctorDialog() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        PrimeFaces.current().dialog().openDynamic("/patient/dialog/SelectDoctor", options, null);
    }

    public void onDoctorChosen(SelectEvent event) {
        Doctor doctor = (Doctor) event.getObject();
        ((Patient) session.getLogin().getUser()).addAllowedDoctor(doctor);
        AbstractUser p = userController.updateUser(session.getLogin().getUser());
        session.getLogin().setUser(p);
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Doktor hinzugefügt", doctor.getUserProfile().getLastname() + ", " + doctor.getUserProfile().getName());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void selectDoctorFromDialog(Doctor d)
    {
        PrimeFaces.current().dialog().closeDynamic(d);
    }
}
