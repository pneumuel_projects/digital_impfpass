/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models.doctor;

import de.pach.entities.vaccinate.Impfstoff;
import de.pach.repositories.ImpfstoffRepo;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PrimeFaces;

/**
 *
 * @author chriss
 */
@SessionScoped
@Named
public class NewVaccineDialogModel implements Serializable {

    @Inject
    ImpfstoffRepo impfstoffRepo;

    private List<Impfstoff> vaccines;
    private List<Impfstoff> filteredVaccines;

    @PostConstruct
    public void init() {
        vaccines = impfstoffRepo.getAllEntities();
    }

    public List<Impfstoff> getVaccines() {
        return vaccines;
    }

    public void setVaccines(List<Impfstoff> vaccines) {
        this.vaccines = vaccines;
    }

    public List<Impfstoff> getFilteredVaccines() {
        return filteredVaccines;
    }

    public void setFilteredVaccines(List<Impfstoff> filteredVaccines) {
        this.filteredVaccines = filteredVaccines;
    }

    public void selectVaccineFromDialog(Impfstoff v) {
        PrimeFaces.current().dialog().closeDynamic(v);
    }
}
