/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models.doctor;

import de.pach.controller.UserController;
import de.pach.entities.roles.AbstractUser;
import de.pach.entities.roles.Doctor;
import de.pach.entities.roles.Patient;
import de.pach.entities.vaccinate.Impfstoff;
import de.pach.entities.vaccinate.Impfung;
import de.pach.models.LoginSessionWrapper;
import de.pach.repositories.UserRepo;
import de.pach.repositories.VaccineRepo;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author chriss
 */
@Named
@SessionScoped
public class PatientOverviewModel implements Serializable {

    @Inject
    LoginSessionWrapper session;

    @Inject
    UserRepo userRepo;
    
    @Inject
    VaccineRepo vaccineRepo;

    @Inject
    UserController userController;

    private Patient selectedUser;
    private List<Patient> patients;
    private List<Patient> filteredPatients;

    private List<Impfung> patientVaccines;
    private List<Impfung> filteredPatientVaccines;
    public List<Impfung> getPatientVaccines() {
        return patientVaccines;
    }

    public void setPatientVaccines(List<Impfung> patientVaccines) {
        this.patientVaccines = patientVaccines;
    }

    public List<Impfung> getFilteredPatientVaccines() {
        return filteredPatientVaccines;
    }

    public void setFilteredPatientVaccines(List<Impfung> filteredPatientVaccines) {
        this.filteredPatientVaccines = filteredPatientVaccines;
    }
    

    @PostConstruct
    public void init() {
        Doctor d = ((Doctor) session.getLogin().getUser());
        patients = d.getPatients();
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    public List<Patient> getFilteredPatients() {
        return filteredPatients;
    }

    public void setFilteredPatients(List<Patient> filteredPatients) {
        this.filteredPatients = filteredPatients;
    }

    public void showVaccineCertificateDialog(Patient p) {
        filteredPatientVaccines = null;
        patientVaccines = p.getUserImpfpass().getImpfungen();
        
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        PrimeFaces.current().dialog().openDynamic("/doctor/dialog/ViewVaccineCertificate", options, null);
    }
    
    public void showNewVaccinationDialog(Patient p) {
        selectedUser = p;
        
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        PrimeFaces.current().dialog().openDynamic("/doctor/dialog/NewVaccineDialog", options, null);
    }

    public void onVaccineChosen(SelectEvent event) {
        Impfstoff impfstoff = (Impfstoff) event.getObject();
        Impfung i = new Impfung();
        i.setImpstoff(impfstoff);
        i.setDoctor((Doctor)session.getLogin().getUser());
        i.setImpfDate(LocalDate.now());
        i = vaccineRepo.create(i);
        selectedUser.getUserImpfpass().addVaccination(i);
        userRepo.updateUser(selectedUser);
        selectedUser = null;
        
        AbstractUser p = userController.updateUser(session.getLogin().getUser());
        session.getLogin().setUser(p);
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Impfung hinzugefügt", "");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void selectDoctorFromDialog(Doctor d) {
        PrimeFaces.current().dialog().closeDynamic(d);
    }
}
