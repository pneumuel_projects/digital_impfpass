/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.controller.RegisterController;
import de.pach.entities.profile.Address;
import de.pach.entities.profile.EGender;
import de.pach.entities.profile.ETitle;
import de.pach.entities.profile.Profile;
import de.pach.entities.profile.UserLogin;
import de.pach.entities.roles.AbstractUser;
import de.pach.entities.roles.Doctor;
import de.pach.entities.roles.Patient;
import de.pach.entities.vaccinate.Impfpass;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.FlowEvent;

/**
 *
 * @author chriss
 */

@Named
@SessionScoped
public class RegisterModel implements Serializable {
    
    private AbstractUser user;
    private Profile userProfile;
    
    private String newPassword = "";
    private String newPasswordAgain = "";
    
    
    
    @Inject
    RegisterController registerController;

    private boolean skip;

    public boolean isSkip() {
        return skip;
    }

    public void save() {
        finalizeRegistration();
        FacesMessage msg = new FacesMessage("Erfolgreich", "Willkommen: " + user.getUserProfile().getName());
        FacesContext.getCurrentInstance().addMessage("", msg);
    }

    public enum Usertype {
        PATIENT, DOCTOR
    };

    private Usertype usertype;

    @PostConstruct
    void init() {
        userProfile = new Profile();
        userProfile.setUserLogin(new UserLogin());
        userProfile.setAddress(new Address());
        usertype = Usertype.PATIENT;

        //Clear Profil
        userProfile.getAddress().setCityname("");
        userProfile.getAddress().setCountry("");
        userProfile.getAddress().setHousenumber("");
        userProfile.getAddress().setZipCode("");
        userProfile.getAddress().setStreetname("");

        userProfile.setLastname("");
        userProfile.setName(null);
        userProfile.setBirthday(LocalDate.now());
    }

    //---------------------------
    //  Profile
    //---------------------------
    public void setUsertype(Usertype usertype) {
        this.usertype = usertype;
    }

    public Usertype getUsertype() {
        return usertype;
    }

    public void setUsername(String username) {
        userProfile.setName(username);
    }

    public String getUsername() {
        return userProfile.getName();
    }

    public void setLastname(String lastname) {
        userProfile.setLastname(lastname);
    }

    public String getLastname() {
        return userProfile.getLastname();
    }

    public void setTitle(ETitle title) {
        userProfile.setTitle(title);
    }

    public ETitle getTitle() {
        return userProfile.getTitle();
    }

    public void setGender(EGender gender) {
        userProfile.setGender(gender);
    }

    public EGender getGender() {
        return userProfile.getGender();
    }

    //---------------------------
    //  Login
    //---------------------------
    public void setEmail(String email) {
        userProfile.getUserLogin().setEmail(email);
    }

    public String getEmail() {
        return userProfile.getUserLogin().getEmail();
    }

    public void setBirthday(LocalDate date) {
        userProfile.setBirthday(date);
    }

    public LocalDate getBirthday() {
        return userProfile.getBirthday();
    }

    public void setPassword(String password) {
        userProfile.getUserLogin().setPassword(password);
    }

    //---------------------------
    //  Address
    //---------------------------
    public void setStreetname(String streetname) {
        userProfile.getAddress().setStreetname(streetname);
    }

    public String getStreetname() {
        return userProfile.getAddress().getStreetname();
    }

    public void setHousenumber(String housenumber) {
        userProfile.getAddress().setHousenumber(housenumber);
    }

    public String getHousenumber() {
        return userProfile.getAddress().getHousenumber();
    }

    public void setCityName(String cityName) {
        userProfile.getAddress().setCityname(cityName);
    }

    public String getCityName() {
        return userProfile.getAddress().getCityname();
    }

    public void setZipcode(String zipcode) {
        userProfile.getAddress().setZipCode(zipcode);
    }

    public String getZipcode() {
        return userProfile.getAddress().getZipCode();
    }

    public void setCountry(String country) {
        userProfile.getAddress().setCountry(country);
    }

    public String getCountry() {
        return userProfile.getAddress().getCountry();
    }

    //---------------------------
    //---------------------------
    public void finalizeRegistration() {
        
        if (null == usertype) {
            throw new NullPointerException("Usertype is null");
        } else {
            switch (usertype) {
                case PATIENT:
                    user = new Patient();
                    ((Patient) user).setUserImpfpass(new Impfpass());
                    break;
                case DOCTOR:
                    user = new Doctor();
                    break;
                default:
                    throw new IllegalArgumentException("UserRole " + usertype.toString() + " is not a valid Role.");
            }
        }
        
        userProfile.getUserLogin().setPassword(newPassword);
        
        userProfile.setVerified(true);
        user.setUserProfile(userProfile);
        
        registerController.finishRegistration(user);
    }

    public boolean validateUser(String s_uuid) {
        UUID uuid = UUID.fromString(s_uuid);
        return registerController.verifyUser(uuid);
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }

    //Gender
    public Object[] getGenders() {
        SelectItem[] items = new SelectItem[EGender.values().length];
        for (int i = 0; i < items.length; i++) {

            items[i] = new SelectItem(EGender.values()[i], EGender.values()[i]
                    .toString().toLowerCase());

        }
        return items;
    }

    //Title
    public Object[] getTitles() {
        SelectItem[] items = new SelectItem[ETitle.values().length];
        for (int i = 0; i < items.length; i++) {

            items[i] = new SelectItem(ETitle.values()[i], ETitle.values()[i]
                    .toString().toLowerCase());

        }
        return items;
    }

    //Usertype
    public Object[] getUsertypes() {
        SelectItem[] items = new SelectItem[Usertype.values().length];
        for (int i = 0; i < items.length; i++) {

            items[i] = new SelectItem(Usertype.values()[i], Usertype.values()[i]
                    .toString().toLowerCase());

        }
        return items;
    }
    
    public void addMessage(String summary, FacesMessage.Severity serverity) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    //SetPassword
    public void checkPassword() {
        if (newPassword.equals("") || newPassword.equals("") || newPasswordAgain.equals("")) {
            
            addMessage("Bitte alle Felder ausfüllen", FacesMessage.SEVERITY_ERROR);
            return;
        }

        if (!newPassword.equals(newPasswordAgain)) {
            addMessage("Die Passwörter stimmen nicht überein", FacesMessage.SEVERITY_ERROR);
            return;

        }
        
        if(newPassword.equals(newPasswordAgain)){
            addMessage("Passwörter passen!", FacesMessage.SEVERITY_INFO);
            setPassword(newPassword);
            
        }
        
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordAgain() {
        return newPasswordAgain;
    }

    public void setNewPasswordAgain(String newPasswordAgain) {
        this.newPasswordAgain = newPasswordAgain;
    }
    

}
