/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models.tests;

import de.pach.entities.Login;
import de.pach.models.LoginSessionWrapper;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@SessionScoped
@Named
public class SessionSharedObjectTestModel implements Serializable {
    
    @Inject
    LoginSessionWrapper loginSessionWraper;
    
    public String doTest()
    {
        if(loginSessionWraper.getLogin() == null){
            return "Fehler: Das Loginobjekt ist null!";
        }
        
        if(loginSessionWraper.getLogin()  == null)
        {
            return "Fehler: Der Nutzer ist null!";
        }
        
        return "Erfolg: Der Nutzer ist gesetzt! ID: " + loginSessionWraper.getLogin().getUser().getId();
    }
    
}
