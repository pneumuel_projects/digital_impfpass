/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models.tests;

import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.time.LocalDate;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Named
@SessionScoped
public class PDFGenerationTestModel implements Serializable {

    private String PDF_STRING;
    private final String PDF_FILE_NAME = "impfpass.pdf";

    public void generatePDF() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        PDF_STRING = externalContext.getRequestServletPath() + "/impfpassPDF.xhtml";
        String servername = externalContext.getRequestServerName();
        String port = String.valueOf(externalContext.getRequestServerPort());
        String appname = externalContext.getRequestContextPath();
        String protocol = externalContext.getRequestScheme();
        HttpSession session = (HttpSession) externalContext.getSession(true);
        String url = protocol + "://" + servername + ":" + port + appname + PDF_STRING + ";jsessionid=" + session.getId();
        try {
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(new java.net.URL(url).toString());
            
            renderer.layout();
            HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
            response.reset();
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline; filename=\"" + PDF_FILE_NAME + "\"");
            OutputStream browserStream = response.getOutputStream();
            renderer.createPDF(browserStream);

        } catch (Exception ex) {
            System.out.println(ex);
        }
        facesContext.responseComplete();
    }
}
