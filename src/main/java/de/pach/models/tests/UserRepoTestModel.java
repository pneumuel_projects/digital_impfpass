package de.pach.models.tests;

import de.pach.crawler.Crawler;
import de.pach.entities.profile.Address;
import de.pach.entities.profile.EGender;
import de.pach.entities.profile.ETitle;
import de.pach.entities.profile.Profile;
import de.pach.entities.profile.UserLogin;
import de.pach.entities.roles.AbstractUser;
import de.pach.entities.roles.Administrator;
import de.pach.entities.roles.Doctor;
import de.pach.entities.roles.Patient;
import de.pach.entities.vaccinate.Impfstoff;
import de.pach.entities.vaccinate.Impfung;
import de.pach.models.LoginSessionWrapper;
import de.pach.repositories.ImpfstoffRepo;
import de.pach.repositories.UserRepo;
import de.pach.repositories.VaccineRepo;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@SessionScoped
@Named
public class UserRepoTestModel implements Serializable {

    private static boolean crawled = false;

    @Inject
    UserRepo userRepo;

    @Inject
    VaccineRepo vaccineRepo;
    
    @Inject
    ImpfstoffRepo impfstoffRepo;

    @Inject
    LoginSessionWrapper loginSessionWraper;

    @Inject
    Crawler crawler;

    public String doTest() {
        if (!crawled) {
            crawler.manualRun();
            crawled = true;
        }

        Patient a = new Patient();
        Doctor d = new Doctor();

        a.setUserProfile(generateUserProfile("Max", "Mustermann"));
        d.setUserProfile(generateUserProfile("Dieter", "Musterdoktor"));

        a = userRepo.addUser(a);
        d = userRepo.addUser(d);
        
        a.addAllowedDoctor(d);
        a = userRepo.updateUser(a);
      
        d = userRepo.getUserByID(d.getClass(), d.getId());

        List<Impfung> impfungen = new ArrayList<Impfung>();
        for (Impfstoff i : impfstoffRepo.getAllEntities()) {
            Impfung im = new Impfung();
            im.setImpfDate(LocalDate.now());
            im.setImpstoff(i);
            im.setDoctor(d);
            impfungen.add(vaccineRepo.create(im));
        }

        a.getUserImpfpass().setImpfungen(impfungen);
        //boolean b = userRepo.constraintValidationsDetected(d);
        //boolean bb = userRepo.constraintValidationsDetected(a);

        
        a = userRepo.updateUser(a);

        loginSessionWraper.getLogin().setUser(d);
        try {
            int res = userRepo.getAll(AbstractUser.class).size();
            System.out.println(res);
            assert (res == 2);
            res = userRepo.getAll(Administrator.class).size();
            System.out.println(res);
            assert (res == 0);
            res = userRepo.getAll(Doctor.class).size();
            System.out.println(res);
            assert (res == 1);
            return "Alles OK!";
        } catch (Exception e) {
            return "Fehler!";
        }
    }

    private Profile generateUserProfile(String name, String lastname) {
        Random r = new Random();

        Profile p = new Profile();
        Address add = new Address();
        p.setGender(EGender.MALE);
        p.setLastname(lastname);
        p.setName(name);
        p.setTitle(ETitle.DR);
        p.setAddress(add);
        p.setVerified(true);
        add.setStreetname("Musterstraße");
        add.setCityname("Musterstadt");
        add.setCountry("Musterland");
        add.setHousenumber(String.valueOf(r.nextInt(99) + 1));
        add.setZipCode(String.valueOf(r.nextInt(55555) + 10000));
        UserLogin l = new UserLogin();
        l.setEmail((name + "." + lastname + "@email.de").toLowerCase());
        l.setPassword(lastname + "passwort");
        p.setUserLogin(l);
        return p;
    }
}
