/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.entities.roles.Patient;
import de.pach.entities.vaccinate.Impfung;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author chriss
 */
@Named
@SessionScoped
public class ImpfpassModel implements Serializable {

    @Inject
    private LoginSessionWrapper loginSessionWraper;

    private List<Impfung> filteredVaccinations;
    private List<Impfung> vaccinations;

    public List<Impfung> getFilteredVaccinations() {
        if (filteredVaccinations != null) {
            System.out.println( this.hashCode() + ": Get Filtered:" + filteredVaccinations.size());
        } else {
            System.out.println(this.hashCode() + ":Get Filtered: null");
        }
        return filteredVaccinations;
    }

    public void setFilteredVaccinations(List<Impfung> filteredVaccinations) {
        if (filteredVaccinations != null) {
            System.out.println(this.hashCode() + ":Filtered:" + filteredVaccinations.size());
        } else {
            System.out.println(this.hashCode() + ":Filtered: null");
        }
        this.filteredVaccinations = filteredVaccinations;
    }

    @PostConstruct
    public void init() {
        Patient p = (Patient) loginSessionWraper.getLogin().getUser();
        vaccinations = p.getUserImpfpass().getImpfungen();
    }

    public List<Impfung> getVaccinations() {
        System.out.println(this.hashCode() + ":Get :" + vaccinations.size());
        return vaccinations;
    }

    public void setVaccinations(List<Impfung> vaccinations) {
        this.vaccinations = vaccinations;
    }
}
