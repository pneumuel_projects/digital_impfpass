/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.controller.LoginController;
import de.pach.controller.UserRole;
import static de.pach.controller.UserRole.*;
import de.pach.entities.Login;
import de.pach.entities.roles.Doctor;
import de.pach.entities.roles.Patient;
import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author chriss
 */
@SessionScoped
@Named
public class LoginModel implements Serializable {

    @Inject
    transient private LoginController loginController;

    @Inject
    private LoginSessionWrapper loginSessionWrapper;

    private String username = "";
    private String password = "";
    private UserRole userRole = UNSPECIFIED;

    //Getter Setter
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public boolean login() {
        if (username.equals("") || password.equals("") || userRole == UNSPECIFIED) {
            return false;
        }

        Login log = loginSessionWrapper.getLogin();
        boolean temp = loginController.
                loginRoleSpecific(username, password, userRole, log);
        log = loginController.reLoginUser(log);
        loginSessionWrapper.setLogin(log);
        if (temp) {
            String startPage;
            if (loginSessionWrapper.getLogin().getUser() instanceof Patient) {
                startPage = "patient/DueVaccinations";
            }
            else if(loginSessionWrapper.getLogin().getUser() instanceof Doctor) {
                startPage = "menue";
            }
            else{
                startPage = "error";
            }

            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(startPage+".xhtml");
            } catch (IOException e) {

            }

        }

        return temp;
    }
    
 

    //Usertype
    public Object[] getUserRoles() {
        SelectItem[] items = new SelectItem[UserRole.values().length];
        for (int i = 0; i < items.length; i++) {

            items[i] = new SelectItem(UserRole.values()[i], UserRole.values()[i]
                    .toString().toLowerCase());

        }
        return items;
    }

    public boolean isLoggedIn() {
        return loginSessionWrapper.getLogin().isLoggedIn();
    }

    public void logout() {
        loginSessionWrapper.setLogin(loginController.
                logoutUser(loginSessionWrapper.getLogin()));
    }

    public String loginWelcome() {
        if (loginSessionWrapper.getLogin().isLoggedIn()) {
            return "Willkommen: " + getUserRole().name() + " " + getUsername();
        } else {
            return "Nicht angemeldet";
        }

    }
}
