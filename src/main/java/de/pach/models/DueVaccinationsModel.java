/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.entities.roles.Patient;
import de.pach.entities.vaccinate.Impfung;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author chriss
 */

@Named
@SessionScoped
public class DueVaccinationsModel implements Serializable
{
    @Inject
    private LoginSessionWrapper loginSession;
    
    public List<Impfung> getDueVaccinations()
    {
        List<Impfung> dueVaccinations = new ArrayList<>();
        for(Impfung impfung : ((Patient)loginSession.getLogin().getUser()).getUserImpfpass().getImpfungen())
        {
            LocalDate nextDueVac;
            nextDueVac = LocalDate.from(impfung.getImpfDate());
            
            //Prüfen ob eine Impfung fällig ist. Die Warnung soll 14 Tage vorher angezeigt werden, daher werden diese auf das heutige Datum aufaddiert
            if(nextDueVac.plus(impfung.getImpstoff().getDuration()).isBefore(LocalDate.now().plusDays(14)))
            {
                dueVaccinations.add(impfung);
            }
        }
        
        return dueVaccinations;
    }
}
