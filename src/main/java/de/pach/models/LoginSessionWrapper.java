/* 
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates 
 * and open the template in the editor. 
 */ 
package de.pach.models; 
 
import de.pach.entities.Login; 
import java.io.Serializable; 
import javax.enterprise.context.SessionScoped; 
import javax.inject.Inject; 
 
/** 
 * 
 * @author Christoph Uhlendorff | Patrick Neumüller 
 */ 
 
/* 
Diese Klasse ist ein Wrapper, welcher eine Login Entity für die Dauer einer Session behält.  
Andere Models können diesen Wraper Injizieren um Zugriff auf der Nutzer der aktuellen Session zu erhalten. 
*/ 
@SessionScoped 
public class LoginSessionWrapper implements Serializable{ 
    @Inject 
    private Login login; 
     
    public Login getLogin() 
    { 
        return login; 
    } 

    public void setLogin(Login login) {
        this.login = login;
    }
    
    
} 