/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.entities.profile.Profile;
import de.pach.entities.roles.Patient;
import de.pach.entities.vaccinate.Impfung;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Named
@SessionScoped
public class ImpfpassPDFModel implements Serializable {
   
    @Inject
    LoginSessionWrapper loginSessionWraper;
    
    private final String errorMessage = "OopsieWoopsie, da ist wohl was schiefgelaufen :(";
    
    public String getFullUsername()
    {
        if(!loginSessionWraper.getLogin().isLoggedIn())
        {
            return errorMessage;
        }
        Profile profile = loginSessionWraper.getLogin().getUser().getUserProfile();
        return profile.getTitle().toString() + ". " + profile.getName() + " " + profile.getLastname();
    }
    
    public String getBirthdate(){
        if(!loginSessionWraper.getLogin().isLoggedIn())
        {
            return errorMessage;
        }
        Profile profile = loginSessionWraper.getLogin().getUser().getUserProfile();
        return profile.getBirthday().toString();
    }
    
    public String getAddressStreet()
    {
        if(!loginSessionWraper.getLogin().isLoggedIn())
        {
            return errorMessage;
        }
        Profile profile = loginSessionWraper.getLogin().getUser().getUserProfile();
        return profile.getAddress().getStreetname() + " " + profile.getAddress().getHousenumber();
    }
    
    public List<Impfung> getVaccinations()
    {
        ArrayList vaccinations;
        vaccinations = new ArrayList<>();
        if(!loginSessionWraper.getLogin().isLoggedIn() || !loginSessionWraper.getLogin().getUser().getClass().equals(Patient.class))
        {
            return vaccinations;
        }
        Patient p = (Patient)loginSessionWraper.getLogin().getUser();
        vaccinations.addAll(p.getUserImpfpass().getImpfungen());
        return vaccinations;
    }
}
