/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.models;

import de.pach.controller.UserController;
import de.pach.entities.profile.EGender;
import de.pach.entities.profile.ETitle;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author chriss
 */
@Named
@SessionScoped
public class UserProfileModel implements Serializable {

    @Inject
    LoginSessionWrapper session;

    @Inject
    UserController userController;

    private String currentPassword = "";
    private String newPassword = "";
    private String newPasswordRepeat = "";

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordRepeat() {
        return newPasswordRepeat;
    }

    public void setNewPasswordRepeat(String newPasswordRepeat) {
        this.newPasswordRepeat = newPasswordRepeat;
    }

    public ETitle[] getTitles() {
        return ETitle.values();
    }

    public void addMessage(String summary, FacesMessage.Severity serverity) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void changeGeneral() {
        session.getLogin().setUser(userController.updateUser(session.getLogin().getUser()));
        addMessage("Die allgemeinen Profilinformationen wurden geändert", FacesMessage.SEVERITY_INFO);
    }

    public void changeAddress() {
        session.getLogin().setUser(userController.updateUser(session.getLogin().getUser()));
        addMessage("Die Adresse wurde erfolgreich geändert", FacesMessage.SEVERITY_INFO);

    }

    public void changePassword() {
        if (currentPassword.equals("") || newPassword.equals("") || newPasswordRepeat.equals("")) {
            System.out.println("CP:" + currentPassword + " NP:" + newPassword + "NPR:" + newPasswordRepeat);
            addMessage("Bitte alle Felder ausfüllen", FacesMessage.SEVERITY_ERROR);
            return;
        }

        if (!newPassword.equals(newPasswordRepeat)) {
            addMessage("Die Passwörter stimmen nicht überein", FacesMessage.SEVERITY_ERROR);
            return;

        }

        if (!session.getLogin().getUser().getUserProfile().getUserLogin().loginCorrect(currentPassword)) {
            addMessage("Das aktuelle Passwort ist nicht korrekt", FacesMessage.SEVERITY_ERROR);
            return;
        }
        session.getLogin().getUser().getUserProfile().getUserLogin().setPassword(newPassword);

        session.getLogin().setUser(userController.updateUser(session.getLogin().getUser()));
        addMessage("Das Passwort wurde erfolgreich geändert", FacesMessage.SEVERITY_INFO);

        currentPassword = "";
        newPassword = "";
        newPasswordRepeat = "";
    }

    public void setUsername(String username) {
        session.getLogin().getUser().getUserProfile().setName(username);
    }

    public String getUsername() {
        return session.getLogin().getUser().getUserProfile().getName();
    }

    public void setLastname(String lastname) {
        session.getLogin().getUser().getUserProfile().setLastname(lastname);
    }

    public String getLastname() {
        return session.getLogin().getUser().getUserProfile().getLastname();
    }

    public void setTitle(ETitle title) {
        session.getLogin().getUser().getUserProfile().setTitle(title);
    }

    public ETitle getTitle() {
        return session.getLogin().getUser().getUserProfile().getTitle();
    }

    public EGender[] getGenders() {
        return EGender.values();
    }

    public void setGender(EGender gender) {
        session.getLogin().getUser().getUserProfile().setGender(gender);
    }

    public EGender getGender() {
        return session.getLogin().getUser().getUserProfile().getGender();
    }

    //---------------------------
    //  Login
    //---------------------------
    public void setEmail(String email) {
        session.getLogin().getUser().getUserProfile().getUserLogin().setEmail(email);
    }

    public String getEmail() {
        return session.getLogin().getUser().getUserProfile().getUserLogin().getEmail();
    }

    public void setPassword(String password) {
        session.getLogin().getUser().getUserProfile().getUserLogin().setPassword(password);
    }

    //---------------------------
    //  Address
    //---------------------------
    public void setStreetname(String streetname) {
        session.getLogin().getUser().getUserProfile().getAddress().setStreetname(streetname);
    }

    public String getStreetname() {
        return session.getLogin().getUser().getUserProfile().getAddress().getStreetname();
    }

    public void setHousenumber(String housenumber) {
        session.getLogin().getUser().getUserProfile().getAddress().setHousenumber(housenumber);
    }

    public String getHousenumber() {
        return session.getLogin().getUser().getUserProfile().getAddress().getHousenumber();
    }

    public void setCityName(String cityName) {
        session.getLogin().getUser().getUserProfile().getAddress().setCityname(cityName);
    }

    public String getCityName() {
        return session.getLogin().getUser().getUserProfile().getAddress().getCityname();
    }

    public void setZipcode(String zipcode) {
        session.getLogin().getUser().getUserProfile().getAddress().setZipCode(zipcode);
    }

    public String getZipcode() {
        return session.getLogin().getUser().getUserProfile().getAddress().getZipCode();
    }

    public void setCountry(String country) {
        session.getLogin().getUser().getUserProfile().getAddress().setCountry(country);
    }

    public String getCountry() {
        return session.getLogin().getUser().getUserProfile().getAddress().getCountry();
    }

    //---------------------------
    //---------------------------
}
