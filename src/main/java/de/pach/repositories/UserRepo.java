/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.repositories;

import de.pach.entities.roles.AbstractUser;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Stateless
public class UserRepo {

    @PersistenceContext(unitName = "impfpassPU")
    private EntityManager em;

    public <T extends AbstractUser> List<T> getAll(Class userClass) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(userClass);
        Root<T> rootEntry = cq.from(userClass);
        CriteriaQuery<T> all = cq.select(rootEntry);
        TypedQuery<T> allQuery = em.createQuery(all);
        return allQuery.getResultList();

        /*Query q =  em.createNamedQuery(AbstractUser.QUERY_GETALL, userClass);
        return q.getResultList();*/
    }

    public <T extends AbstractUser> T addUser(T user) {
        em.persist(user);
        em.flush();
        return user;
    }

    public <T extends AbstractUser> T updateUser(T user) {
        T u = em.merge(user);
        em.flush();
        return u;
    }

    public <T extends AbstractUser> boolean removeUser(T user) {
        try {
            em.remove(user);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public <T extends AbstractUser> T getUserByID(Class<T> userClass, long id) {
        return em.find(userClass, id);
    }

    public <T extends AbstractUser> T getUserByUUID(Class<T> userClass, UUID uuid) {
        Query q = em.createNamedQuery(AbstractUser.QUERY_GETBYUUID, userClass);
        q.setParameter("uuid", uuid);
        //q.setParameter("class", userClass.getName());
        return userClass.cast(q.getSingleResult());
    }

    public <T extends AbstractUser> T getUserByEMAIL(Class<T> userClass, String email) {
        try {
            Query q = em.createNamedQuery(AbstractUser.QUERY_GETBYEMAIL, userClass);
            q.setParameter("email", email);
            //q.setParameter("class", userClass.getName());
            return userClass.cast(q.getSingleResult());
        } catch (NoResultException e) {
            return null;
        }
    }
}
