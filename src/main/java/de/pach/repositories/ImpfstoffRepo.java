/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.repositories;

import de.pach.crawler.PageConstant;
import de.pach.entities.vaccinate.Impfstoff;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Stateless
public class ImpfstoffRepo {

    @PersistenceContext(unitName = "impfpassPU")
    private EntityManager em;

    public Impfstoff create(Impfstoff entity) {
        em.persist(entity);
        em.flush();
        return entity;
    }

    public Impfstoff update(Impfstoff entity) {
        return em.merge(entity);
    }

    public void remove(long id) {
        Impfstoff impfstoffDelete = getEntity(id);
        em.remove(impfstoffDelete);
    }

    public Impfstoff getEntity(long id) {
        return em.find(Impfstoff.class, id);
    }

    public List<Impfstoff> getAllEntities() {
        return em.createNamedQuery(Impfstoff.QUERY_GETALL, Impfstoff.class)
                .getResultList();
    }

    public Impfstoff findByName(String s) {
        try {
            final Query q = em.createNamedQuery(Impfstoff.QUERY_FINDBYNAME,
                    Impfstoff.class);
            q.setParameter("zulassungsnummer", s);
            return (Impfstoff) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public boolean isName(Impfstoff impfstoff) {
        return findByName(impfstoff.getZulassungsnummer()) != null;
    }

    public void dropTable() {
        em.createQuery("DELETE FROM Impfstoff i").executeUpdate();
    }

    public List<Impfstoff> getAllKrankheit(PageConstant pc) {
        try {
            final Query q = em.createNamedQuery(Impfstoff.QUERY_GETALLKRANKHEIT,
                    Impfstoff.class);
            q.setParameter("krankheit", pc);
            return q.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
}
