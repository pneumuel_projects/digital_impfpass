/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.repositories;

import de.pach.entities.vaccinate.Impfung;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author chriss
 */
@Stateless
public class VaccineRepo {
    
    @PersistenceContext(unitName = "impfpassPU")
    private EntityManager em;
    
    public Impfung create(Impfung entity) {
        em.persist(entity);
        em.flush();
        return entity;
    }
    
    
    
    public Impfung update(Impfung entity) {
        return em.merge(entity);
    }

    public void remove(long id) {
        Impfung impfstoffDelete = getEntity(id);
        em.remove(impfstoffDelete);
    }
    
     public Impfung getEntity(long id) {
        return em.find(Impfung.class, id);
    }
}
