/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.repositories;


import de.pach.entities.Login;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Stateless
public class LoginCrud {

    @PersistenceContext(unitName = "impfpassPU")
    private EntityManager entityManager;

    public Login getEntity(long id) {
        try{
        return entityManager.find(Login.class, id);
        } catch(NoResultException e){
            return null;
            
        }
    }
    
    public Login create(Login entity) {
        entityManager.persist(entity);
        entityManager.flush();
        return entity;
    }
    
    public Login update(Login entity) {
        
        Login log = entityManager.merge(entity);
        entityManager.flush();
        return log;
    }
}

