/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@MappedSuperclass
public abstract class EntityWithId implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //Optimistischer Lock Mode
    @Version
    private Timestamp lastChanged;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public EntityWithId()
    {
        super();
        lastChanged = Timestamp.valueOf(LocalDateTime.now());
        id = 0L;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntityWithId)) {
            return false;
        }
        EntityWithId other = (EntityWithId) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "de.pach.entities.EntityWithId[ id=" + id + " ]";
    }
    
}
