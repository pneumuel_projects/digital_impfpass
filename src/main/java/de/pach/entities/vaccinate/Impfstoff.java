/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.vaccinate;

import de.pach.crawler.PageConstant;
import de.pach.entities.EntityWithId;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = Impfstoff.QUERY_GETALL,
            query = "SELECT i FROM Impfstoff i")
    ,
    @NamedQuery(
            name = Impfstoff.QUERY_FINDBYNAME,
            query = "SELECT i FROM Impfstoff i WHERE i.zulassungsnummer ="
            + " :zulassungsnummer")
    ,
    @NamedQuery(
            name = Impfstoff.QUERY_GETALLKRANKHEIT,
            query = "SELECT i FROM Impfstoff as i WHERE i.krankheit = :krankheit"),})

public class Impfstoff extends EntityWithId implements Serializable {

    public static final String QUERY_GETALL = "Impfstoff.GetAll";
    public static final String QUERY_FINDBYNAME = "Impfstoff.FindByName";
    public static final String QUERY_GETALLKRANKHEIT = "Impfstoff.GetAllKrankheit";

    @Enumerated(EnumType.STRING)
    private PageConstant krankheit;

    private String beschreibung;

    @Lob
    @Column(name = "indikationsgruppe", length = 512)
    private String indikationsgruppe;

    private Period duration;

    private String zulassungsInhaber;

    @Enumerated(EnumType.STRING)
    private EImpfstoffart impfstoffart;

    private String zulassungsnummer;

    private LocalDate zulassungsDatum;

    private String informationen;

    public Impfstoff() {
        super();
        krankheit = PageConstant.UNKOWN;
        beschreibung = "unknown";
        indikationsgruppe = "unknown";
        zulassungsInhaber = "unknown";
        impfstoffart = EImpfstoffart.UNKOWN;
        zulassungsnummer = "0";
        zulassungsDatum = LocalDate.MIN;
        informationen = "unknown";
        duration = Period.ZERO;
    }

    //Getter Setter
    //----------------------------------------------
    @Override
    public String toString() {
        return "de.pach.entities.impfstoff.Impfstoff" + " id: " + getId();
    }

    public Period getDuration() {
        return duration;
    }

    public void setDuration(Period duration) {
        this.duration = duration;
    }

    public PageConstant getKrankheit() {
        return krankheit;
    }

    public void setKrankheit(PageConstant krankheit) {
        this.krankheit = krankheit;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getIndikationsgruppe() {
        return indikationsgruppe;
    }

    public void setIndikationsgruppe(String indikationsgruppe) {
        this.indikationsgruppe = indikationsgruppe;
    }

    public String getZulassungsInhaber() {
        return zulassungsInhaber;
    }

    public void setZulassungsInhaber(String zulassungsInhaber) {
        this.zulassungsInhaber = zulassungsInhaber;
    }

    public EImpfstoffart getImpfstoffart() {
        return impfstoffart;
    }

    public void setImpfstoffart(EImpfstoffart impfstoffart) {
        this.impfstoffart = impfstoffart;
    }

    public String getZulassungsnummer() {
        return zulassungsnummer;
    }

    public void setZulassungsnummer(String zulassungsnummer) {
        this.zulassungsnummer = zulassungsnummer;
    }

    public LocalDate getZulassungsDatum() {
        return zulassungsDatum;
    }

    public void setZulassungsDatum(LocalDate zulassungsDatum) {
        this.zulassungsDatum = zulassungsDatum;
    }

    public String getInformationen() {
        return informationen;
    }

    public void setInformationen(String informationen) {
        this.informationen = informationen;
    }

}
