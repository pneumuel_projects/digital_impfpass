/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.vaccinate;

import de.pach.entities.EntityWithId;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
public class Impfpass extends EntityWithId implements Serializable {
    
    @OneToMany(cascade = CascadeType.ALL)
    private List<Impfung> impfungen;
    
    public Impfpass(){
        impfungen = new ArrayList<>();
    }
    
    public void addVaccination(Impfung vac)
    {
        impfungen.add(vac);
    }
    
    @Override
    public String toString() {
        return "de.pach.entities.impfen.Impfpass" + " id: " + getId();
    }

    //Getter Setter 
    //_____________________________________________
    
    public List<Impfung> getImpfungen() {
        return impfungen;
    }

    public void setImpfungen(List<Impfung> impfungen) {
        this.impfungen = impfungen;
    }
    
    
    
}
