/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.vaccinate;

import de.pach.entities.EntityWithId;
import de.pach.entities.roles.Doctor;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
public class Impfung extends EntityWithId implements Serializable {

    @NotNull
    private LocalDate impfDate;
    
    @NotNull
    @ManyToOne
    private Impfstoff impstoff;
    
    @NotNull
    @ManyToOne
    private Doctor doctor;

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }
    
    public Impfung()
    {
        super();
        impfDate = LocalDate.MIN;
        impstoff = null;
    }
    
    @Override
    public String toString() {
        return "de.pach.entities.impfen.Impfungen" + " id: " + getId();
    }

    
    //Getter Setter
    //----------------------------------------------
    
    public LocalDate getImpfDate() {
        return impfDate;
    }

    public void setImpfDate(LocalDate impfDate) {
        this.impfDate = impfDate;
    }

    public Impfstoff getImpstoff() {
        return impstoff;
    }

    public void setImpstoff(Impfstoff impstoff) {
        this.impstoff = impstoff;
    }
    
    
    
}
