/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.profile;

import de.pach.entities.EntityWithId;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
public class Profile extends EntityWithId implements Serializable {

    @NotNull(message = "Vorname darf nicht leer sein")
    @Size(min=3, max=20 , message = "Muss mindestens 3 und maximal 20 Zeichen lang sein")
    private String name;
    
    @NotNull(message = "Nachname darf nicht leer sein")
    @Size(min=3, max=20, message = "Muss mindestens 3 und maximal 20 Zeichen lang sein")
    private String lastname;
    
    //Pattern für Date
    @NotNull(message = "Geburtsdatum darf nicht leer sein")
    private LocalDate birthday;
    
    //orphanRemoval auf false damit die email blockiert wird
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = false)
    private UserLogin userLogin;
    
    //@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @Embedded
    @Enumerated(EnumType.STRING)
    private ETitle title;
 
    //@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @Embedded
    @Enumerated(EnumType.STRING)
    private EGender gender;
    
    
    @Embedded
    private Address address;
    
    private boolean verified;
    
    private UUID userUUID;
    
    public Profile()
    {
        super();
        name = "unknown";
        lastname = "unknown";
        userLogin = new UserLogin();
        address = new Address();
        title = ETitle.NOTHING;
        gender = EGender.UNKOWN;
        userUUID = UUID.randomUUID();
        verified = false;
        birthday = LocalDate.MIN;
    }
    
    public UUID getUserUUID()
    {
        return userUUID;
    }
    
    public void setUserUUID(UUID uuid)
    {
        this.userUUID = uuid;
    }
    
    public Address getAddress()
    {
        return address;
    }
    
    public void setAddress(Address address)
    {
        this.address = address;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
    
    
    //ToString
    @Override
    public String toString() {
        return "de.pach.entities.profile.Profile"  + " id: " + getId();
    }

    //Getter Setter
    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public ETitle getTitle() {
        return title;
    }

    public void setTitle(ETitle title) {
        this.title = title;
    }

    public EGender getGender() {
        return gender;
    }

    public void setGender(EGender gender) {
        this.gender = gender;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
    
    
}
