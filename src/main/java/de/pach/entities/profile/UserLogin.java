/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.profile;

import de.pach.entities.EntityWithId;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
public class UserLogin extends EntityWithId implements Serializable {

    private byte[] salt;
    private byte [] password;
    
    public UserLogin()
    {
        super();
        salt = null;
        password = null;
    }
    
    /*
    * regex fuer email verifizierung 
    * @see https://docs.oracle.com/cd/E19798-01/821-1841/gkahq/index.html
    */
    @NotNull(message = "Email darf nicht leer sein")
    @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
        +"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
        +"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
             message="{invalid.email}")
    private String email;
    
    // TODO Password Hash 
    public byte[] hashPassword( String password ) {
        int iterations = 10000;
        int keyLength = 128;
        
       try {
           SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
           PBEKeySpec spec = new PBEKeySpec( password.toCharArray(), getSalt(), iterations, keyLength );
           SecretKey key = skf.generateSecret( spec );
           byte[] res = key.getEncoded( );
           return res;
       } catch( NoSuchAlgorithmException | InvalidKeySpecException e ) {
           throw new RuntimeException( e );
       }
   }
    

    
    //Getter Setter
    private byte [] getSalt()
    {
        if(salt == null || salt.length != 20){
        SecureRandom random = new SecureRandom();
        salt = new byte[20];
        random.nextBytes(salt);
        }
        return salt;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getPassword() {
        return password;
    }

    public boolean loginCorrect(String password)
    {
      return Arrays.equals(hashPassword(password), this.password);     
    }
    
    public void setPassword(String password) {
        this.password = hashPassword(password);
    }
}
