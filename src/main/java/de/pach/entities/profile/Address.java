/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.profile;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Embeddable
public class Address implements Serializable {

    @NotNull(message = "Postleitzahl darf nicht leer sein")
    @Pattern(regexp = ".*[^0-9].*")
    @Size(min=5, max=5, message = "Muss mindestens 5 und maximal 5 Zeichen lang sein")
    private String zipCode;
    
    @NotNull(message = "Straßenname darf nicht leer sein")
    @Size(min=3, max=20, message = "Muss mindestens 3 und maximal 20 Zeichen lang sein")
    private String streetname;
    
    @NotNull(message = "Hausnummer darf nicht leer sein")
    @Pattern(regexp = ".*[^0-9].*")
    @Size(min=1, max=4, message = "Muss mindestens 1 und maximal 3 Zeichen lang sein")
    private String housenumber;
    
    @NotNull(message = "Stadt darf nicht leer sein")
    @Size(min=3, max=20 , message = "Muss mindestens 3 und maximal 20 Zeichen lang sein")
    private String cityname;
    
    @NotNull(message = "Land darf nicht leer sein")
    @Size(min=3, max=40, message = "Muss mindestens 3 und maximal 40 Zeichen lang sein")
    private String country;

    public Address()
    {
        super();
        zipCode = "00000";
        streetname = "unknown";
        housenumber = "0000";
        cityname = "unknown";
        country = "unknown";
    }
    
    //ToString
    @Override
    public String toString() {
        return "de.pach.entities.profile.Address";
    }

    
    //Getter Setter
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreetname() {
        return streetname;
    }

    public void setStreetname(String streetname) {
        this.streetname = streetname;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }  
}
