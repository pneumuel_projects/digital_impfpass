/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities;

import de.pach.entities.roles.AbstractUser;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
public class Login extends EntityWithId implements Serializable {
    
    
    private Timestamp lastLoginAttempt;
    
    @Min(value=0, message="Anzahl der Loginversuche kann nicht negativ sein.")
    private int failedLoginAttempts;
    
    //optional = true - nicht jedes Login hat bereits einen Nutzer
    @ManyToOne(cascade = CascadeType.MERGE, optional=true)
    private AbstractUser user;

    public Login()
    {
        super();
        lastLoginAttempt = Timestamp.valueOf(LocalDateTime.MIN);
        failedLoginAttempts = 0;
        user = null;
    }
    
    // Getter, Setter
    //-------------------------------
    
    public AbstractUser getUser() {
        return user;
    }

    public void setUser(AbstractUser user) {
        this.user = user;
    }
    
    public LocalDateTime getLastLoginAttempt() {
        return lastLoginAttempt.toLocalDateTime();
    }

    public void setLastLoginAttempt(LocalDateTime lastLogin) {
        this.lastLoginAttempt = Timestamp.valueOf(lastLogin);
    }

    public int getFailedLoginAttempts() {
        return failedLoginAttempts;
    }

    public void setFailedLoginAttempts(int failedLoginAttempts) {
        this.failedLoginAttempts = failedLoginAttempts;
    }
    
    //------------------------------------
    
    public boolean isLoggedIn()
    {
        return user != null;
    }
    
    public void increaseFailedLoginAttempts() {
        this.failedLoginAttempts++;
    }
    
    @Override
    public String toString() {
        return "de.pach.entities.Login[ id=" + getId() + " ]";
    }
    
}
