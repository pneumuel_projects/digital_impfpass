/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.roles;

import de.pach.entities.EntityWithId;
import de.pach.entities.profile.Profile;
import java.io.Serializable;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Inheritance;
import static javax.persistence.InheritanceType.SINGLE_TABLE;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */

@Entity
@Table(name="USERS")
@Inheritance(strategy=SINGLE_TABLE)
@DiscriminatorValue("AUSER")
@DiscriminatorColumn(name = "USER_TYPE")
@NamedQueries({
    @NamedQuery(name = AbstractUser.QUERY_GETALL, query="SELECT u FROM AbstractUser u"),
    @NamedQuery(name = AbstractUser.QUERY_GETBYUUID, query="SELECT u FROM AbstractUser u WHERE u.userProfile.userUUID = :uuid"),
    @NamedQuery(name =AbstractUser.QUERY_GETBYEMAIL, query="SELECT u FROM AbstractUser u WHERE u.userProfile.userLogin.email = :email"),
})
public abstract class AbstractUser extends EntityWithId implements Serializable {
    
    public static final String QUERY_GETALL = "AbstractUser.GetAll";
    public static final String QUERY_GETBYUUID = "AbstractUser.GetByUUID";
    public static final String QUERY_GETBYEMAIL = "AbstractUser.GetByEMAIL";
    
    @NotNull
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Profile userProfile;

    public AbstractUser()
    {
        super();
        userProfile = null;
    }
    
    //Getter Setter
    //-----------------------------------------------
    
    public Profile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(Profile userProfile) {
        this.userProfile = userProfile;
    }
    
    
}
