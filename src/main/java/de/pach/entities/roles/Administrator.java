/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.roles;

import de.pach.entities.EntityWithId;
import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
@DiscriminatorValue("ADMINISTRATOR")
public class Administrator extends AbstractUser implements Serializable {

    public Administrator()
    {
        super();
    }
    
    @Override
    public String toString() {
        return "de.pach.entities.roles.Admin" + " id: " + getId();
    }
    
}
