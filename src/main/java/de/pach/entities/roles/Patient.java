/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.roles;

import de.pach.entities.vaccinate.Impfpass;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
@DiscriminatorValue("PATIENT")
public class Patient extends AbstractUser implements Serializable {

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Impfpass userImpfpass;

    //TODO Prüfen ob die betroffenen Doktoren beim löschen des Nutzers auch gelöscht werden.
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="DOC_PATIENTS",joinColumns=@JoinColumn(name="P_ID"),
      inverseJoinColumns=@JoinColumn(name="DOC_ID"))
    private List<Doctor> allowedDoctors;

    public Patient() {
        super();
        userImpfpass = new Impfpass();
        allowedDoctors = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "de.pach.entities.roles.Patient" + " id: " + getId();
    }

    //Getter Setter
    //-----------------------------------------------
    public Impfpass getUserImpfpass() {
        return userImpfpass;
    }

    public void setUserImpfpass(Impfpass userImpfpass) {
        this.userImpfpass = userImpfpass;
    }

    public List<Doctor> getAllowedDoctors() {
        return allowedDoctors;
    }

    public void removeAllowedDoctor(Doctor doctor) {
        allowedDoctors.remove(doctor);
    }

    public void addAllowedDoctor(Doctor doctor) {
        if (!allowedDoctors.contains(doctor)) {
            allowedDoctors.add(doctor);
        }
    }

    public void setAllowedDoctors(List<Doctor> allowedDoctors) {
        this.allowedDoctors = allowedDoctors;
    }

}
