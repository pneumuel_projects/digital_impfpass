/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.pach.entities.roles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author Christoph Uhlendorff | Patrick Neumüller
 */
@Entity
@DiscriminatorValue("DOCTOR")
public class Doctor extends AbstractUser implements Serializable {

    @ManyToMany(mappedBy="allowedDoctors", fetch = FetchType.EAGER)
    private List<Patient> patients;
    
    public Doctor()
    {
        super();
        patients = new ArrayList<>();
    }
    
    @Override
    public String toString() {
        return "de.pach.entities.roles.Doctor" + " id: " + getId();
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }  
}
